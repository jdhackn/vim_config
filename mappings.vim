" Mapping declaractions

" Sudo access if you forgot to use sudo vim to open a file, use :w!! to save
cmap w!! %!sudo tee > /dev/null %

" Additional buffer changing keys
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Shortcut to my Vim config file
nnoremap <leader>evs <C-w>s<C-w>j:e ~/.vim/settings.vim<cr>
nnoremap <leader>evm <C-w>s<C-w>j:e ~/.vim/mappings.vim<cr>
nnoremap <leader>evb <C-w>s<C-w>j:e ~/.vim/bundles.vim<cr>
nnoremap <leader>ezz <C-w>s<C-w>j:e ~/.zshrc<cr>

" RSpec mappings
map <leader>t :call RunCurrentSpecFile()<cr>
map <leader>s :call RunNearestSpec()<cr>
map <leader>p :call RunLastSpec()<cr>
map <leader>a :call RunAllSpecs()<cr>

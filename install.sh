#!/bin/bash

DEFAULT_VIMDIR=$HOME/.vim

if [ -z $VIMDIR ]; then
  echo "Environment variable VIMDIR not present, setting to default path: $DEFAULT_VIMDIR"
  export VIMDIR=$DEFAULT_VIMDIR
else
  echo "Environment variable VIMDIR already set, using it for vim config: $VIMDIR"
fi

echo "Cloning the vim configuration..."
git clone https://bitbucket.org/A771cu5/vim_config.git $VIMDIR

if [ -z $ZSH_NAME ]; then
  rc_file=".bashrc"
  echo "Zsh not detected, attempting to use bashrc."
else
  rc_file=".zshrc"
  echo "Zsh detected - using zshrc file."
fi

echo "export VIMDIR=$VIMDIR" >> $HOME/$rc_file

git clone https://github.com/VundleVim/Vundle.vim $VIMDIR/bundle/Vundle.vim
vim -u $VIMDIR/bundles.vim +BundleInstall! +qa!

echo "Vim configuration complete!"
